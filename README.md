# 14th Industrial Problem Solving Workshop
## Problem from: Authorité des marchés financiers

Date: May 13 to 17, 2024


Contributors: Jacky Jang, Helen Samara Dos Santos, Lingyi Yang, Odile Marcotte, Rami Younes, Philippe Béliveau

# Problem Statement
DeFi transactions can get complex, especially when involving multiple smart contracts, which can quickly increase the number of sub-transactions.

The objective of this project is to effectively trace and explain these transactions, with a particular interest in what is **sent out**, what is **received**, and what the **value** of the transaction is in Wrapped Ether (WETH).

# Proposed Solution

The current solution by the Authorité des marchés financiers (AMF) is limited by transaction size, and follows a strictly tabular approach.

We propose instead a graphical approach, looking specifically for an edge cut forming a bipartite graph, where only edges transferring WETH connect source nodes to destination nodes (sinks). In situations where we fail to produce such a structure, we declare the transaction as untraceable. (Implementation can be found in the file helen.py)

# Results
- We achieved a 97.36% accuracy in reproducing the traces provided by the AMF.

- The proposed solution has very few false positives:
	- In most cases where our algorithm was wrong, it yielded no results (i.e. declared the transaction as untraceable)
	- In some cases where we were wrong, we concluded that the AMF's solution was the faulty one.
	- 
# Conclusion
We proposed a graphical approach to tracing transactions on the Ethereum blockchain, which yields reliable and deterministic solutions. 

Whereas the AMF's solution is restricted by transaction size, ours is instead sensitive to the underlying graphical structure; There are many situations where a simplification of the graph (for example with respect to pool and swap nodes) will yield a valid bipartite graph when it previously considered the transaction as untraceable.

# Example
The following is an example of a transaction where we disagreed with the AMF, and where a simplification of the graph structure allowed us to find an edge cut. Note that every graph shown here is also available under an interactive format in the folder /case/

Hash: 0x1e31ec14ac0b28d831a9f2b808f85d00594c77ee9e07d07ea72d3d505aa25dcc


Transaction on the blockchain: https://basescan.org/tx/0x1e31ec14ac0b28d831a9f2b808f85d00594c77ee9e07d07ea72d3d505aa25dcc


## 1- Graphical representation of the transaction
![base graph](case/base_graph.png)

## 2- Application of the solution (untraceable)
![untraceable](case/untraceable_graph.png)

We cannot find an edge cut due to the edge (a,b) not being expressed in WETH.

## 3- Edge contracted graph (solution found)
![solved](case/contracted_graph.png)

We found the edge cut {(j,a), (e,a)} for a total transaction value of 2.411 WETH



